﻿using System;

namespace DZ_02_26._11._2020
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1.
            Console.WriteLine("12  21  15");
            Console.WriteLine("-----------");

            // 2.
            Console.WriteLine("5");
            Console.WriteLine("10");
            Console.WriteLine("21");
            Console.WriteLine("-----------");

            // 3. 
            Console.Write("Введите значение в сантиметрах: ");
            double valueInCentimeters = double.Parse(Console.ReadLine());
            double valueInMeters = valueInCentimeters / 100;
            Console.WriteLine("Значение в метрах: " + valueInMeters);
            Console.WriteLine("-----------");

            // 4.
            Console.Write("За 234 дня прошло полных недель: ");
            int countOfWeeks = 234 / 7;
            Console.WriteLine(countOfWeeks);
            Console.WriteLine("-----------");

            // 5.
            Console.Write("Введите двухзначное число: ");
            int number = int.Parse(Console.ReadLine());
            if (number > 9 && number < 100)
            {
                int numberOfTeens = number / 10;
                int numberOfUnits = number % 10;
                int sumOfDigits = numberOfTeens + numberOfUnits;
                int productOfNumbers = numberOfTeens * numberOfUnits;
                Console.Write("a) число десятков в нем - " + numberOfTeens +
                    "\nb) число единиц в нем - " + numberOfUnits +
                    "\nc) сумму его цифр - " + sumOfDigits +
                    "\nd) произведение его цифр - " + productOfNumbers+"\n");
            }
            else
                Console.WriteLine("Было введено некорректное число");
            Console.WriteLine("-----------");

            // 6.
            bool A = true, B = false, C = false;
            bool resultA = A || B;
            bool resultB = A && B;
            bool resultC = B || C;
            Console.WriteLine("a) " + resultA + "\nb) " + resultB + "\nc) " + resultC);
            Console.WriteLine("-----------");

            // 7.
            Console.Write("Введите радиус круга: ");
            double radiusCircle = double.Parse(Console.ReadLine());
            Console.Write("Введите значение стороны квадрата: ");
            double sideOfSquare = double.Parse(Console.ReadLine());
            double areaOfCircle = Math.PI * Math.Pow(radiusCircle, 2);
            double suqareArea = Math.Pow(sideOfSquare, 2);
            if (areaOfCircle > sideOfSquare)
            {
                Console.WriteLine("Площадь круга больше площади квадрата");
            }
            else if (areaOfCircle < sideOfSquare)
            {
                Console.WriteLine("Площадь квадрата больше площади круга");
            }
            else if (areaOfCircle == sideOfSquare)
            {
                Console.WriteLine("Площадь круга равна площади квадрата");
            }
            Console.WriteLine("-----------");

            // 8.
            Console.Write("Введите массу тела №1: ");
            double bodyWeightFirst = double.Parse(Console.ReadLine());
            Console.Write("Введите объем тела №1: ");
            double bodyVolumeFirst = double.Parse(Console.ReadLine());
            Console.Write("Введите массу тела №2: ");
            double bodyWeightSecond = double.Parse(Console.ReadLine());
            Console.Write("Введите объем тела №2: ");
            double bodyVolumeSecond = double.Parse(Console.ReadLine());
            double bodyDensityFirst = bodyWeightFirst / bodyVolumeFirst;
            double bodyDensitySecond = bodyWeightSecond / bodyVolumeSecond;
            if (bodyDensityFirst > bodyDensitySecond)
            {
                Console.WriteLine("Плотность первого тела больше плотности второго тела");
            }
            else if (bodyDensityFirst < bodyDensitySecond)
            {
                Console.WriteLine("Плотность второго тела больше плотности первого тела");
            }
            else if (bodyDensityFirst == bodyDensitySecond)
            {
                Console.WriteLine("Плотности тел равны");
            }
            Console.WriteLine("-----------");


            // 9.

            Console.Write("Введите сопротивление №1: ");
            double resistanceFirst = double.Parse(Console.ReadLine());
            Console.Write("Введите сопротивление №2: ");
            double resistanceSecond = double.Parse(Console.ReadLine());
            Console.Write("Введите напряжение №1: ");
            double voltageFirst = double.Parse(Console.ReadLine());
            Console.Write("Введите напряжение №2: ");
            double voltageSecond = double.Parse(Console.ReadLine());
            double amperageFirst = voltageFirst / resistanceFirst;
            double amperageSecond = voltageSecond / resistanceSecond;
            if (amperageFirst > amperageSecond)
            {
                Console.WriteLine("По второму участку протекает меньший ток");
            }
            else if (amperageFirst < amperageSecond)
            {
                Console.WriteLine("По первому участку протекает меньший ток");
            }
            else if (amperageFirst == amperageSecond)
            {
                Console.WriteLine("Сила тока равна на обоих участках");
            }
            Console.WriteLine("-----------");


            // 10.
            int integerA = 20;
            while (integerA <= 35)
            {
                Console.WriteLine(integerA);
                integerA++;
            }
            Console.WriteLine("-----------");
            int squaresOfIntegers = 10;
            Console.Write("Введите значение b: ");
            int b = int.Parse(Console.ReadLine());
            if (b > 10)
            {
                while (squaresOfIntegers <= b)
                {
                    Console.WriteLine(Math.Pow(squaresOfIntegers, 2));
                    squaresOfIntegers++;
                }
            }
            else
                Console.WriteLine("Введено значение меньше 10!");
            Console.WriteLine("-----------");
            Console.Write("Введите значение a: ");
            int a = int.Parse(Console.ReadLine());
            if (a < 50)
            {
                while (a <= 50)
                {
                    Console.WriteLine(Math.Pow(a, 3));
                    a++;
                }
            }
            else
                Console.WriteLine("Введено значение больше 50!");
            Console.WriteLine("-----------");
            Console.Write("Введите значение a: ");
            int valueA = int.Parse(Console.ReadLine());
            Console.Write("Введите значение b: ");
            int valueB = int.Parse(Console.ReadLine());
            if (valueB > valueA)
            {
                while (valueA <= valueB)
                {
                    Console.WriteLine(valueA);
                    valueA++;
                }
            }
            else
                Console.WriteLine("Введен некорректный диапазон");




        }
    }
}
